// берём Express
var express = require('express');
// создаём Express-приложение
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
var pg = require("pg");
var connect = "postgres://postgres:@localhost:5432/old";

    app.get('/', function (req, res) {
        


    pg.connect(connect, function(err, client, done) {

        if (err) {
            return console.error('error fetching client from pool', err);
        }
        client.query("SELECT * FROM first", function(err, result) {
            if (err) {
                return console.error('error running query', err);
            }
            res.render('index', {recipes: result.rows});
            done();
             });
        });

    });

// запускаем сервер на порту 8080
app.listen(8080);
// отправляем сообщение
console.log('Сервер стартовал!');